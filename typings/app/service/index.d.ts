// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
type AnyClass = new (...args: any[]) => any;
type AnyFunc<T = any> = (...args: any[]) => T;
type CanExportFunc = AnyFunc<Promise<any>> | AnyFunc<IterableIterator<any>>;
type AutoInstanceType<T, U = T extends CanExportFunc ? T : T extends AnyFunc ? ReturnType<T> : T> = U extends AnyClass ? InstanceType<U> : U;
import ExportMall from '../../../app/service/Mall';
import ExportTest from '../../../app/service/Test';
import ExportMallBaseMall from '../../../app/service/Mall/BaseMall';
import ExportMallJDMall from '../../../app/service/Mall/JDMall';
import ExportMallPDDMall from '../../../app/service/Mall/PDDMall';
import ExportMallSNMall from '../../../app/service/Mall/SNMall';
import ExportMallTBMall from '../../../app/service/Mall/TBMall';
import ExportMallTypingMall from '../../../app/service/Mall/typing/Mall';

declare module 'egg' {
  interface IService {
    mall: ExportMall & {
      baseMall: AutoInstanceType<typeof ExportMallBaseMall>;
      jDMall: AutoInstanceType<typeof ExportMallJDMall>;
      pDDMall: AutoInstanceType<typeof ExportMallPDDMall>;
      sNMall: AutoInstanceType<typeof ExportMallSNMall>;
      tBMall: AutoInstanceType<typeof ExportMallTBMall>;
      typing: {
        mall: AutoInstanceType<typeof ExportMallTypingMall>;
      }
    }
    test: AutoInstanceType<typeof ExportTest>;
  }
}
