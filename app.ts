import dayjs = require('dayjs');
import utc = require('dayjs/plugin/utc'); // dependent on utc plugin
import timezone = require('dayjs/plugin/timezone');

dayjs.extend(utc);
dayjs.extend(timezone);
