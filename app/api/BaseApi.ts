import Axios, { AxiosRequestConfig } from 'axios';

export default class BaseApi {
  request(params: IRequestParams) {
    return Axios.request(params);
  }
}

interface IRequestParams extends AxiosRequestConfig {}
