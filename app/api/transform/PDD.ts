import { Platform } from '../../service/Mall/typing/Mall';

export default class PDDTransform {
  static search(dataList: any) {
    return dataList.map(data => ({
      id: data.goods_id,
      platform: Platform.PDD, // 平台
      title: data.goods_name, // 标题
      mainPic: data.goods_image_url, // 主图
      originalPrice: data.min_normal_price / 100, // 原价
      actualPrice: data.min_group_price / 100, // 卷后价(真实价格)
    }));
  }
}
