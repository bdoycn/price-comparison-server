import { Platform } from '../../service/Mall/typing/Mall';
import { get } from 'lodash';

export default class SNTransform {
  static search(dataList: any) {
    return dataList.map(data => ({
      id: data.commodityInfo.commodityCode,
      platform: Platform.SN, // 平台
      title: data.commodityInfo.commodityName, // 标题
      mainPic: get(data, [ 'commodityInfo', 'pictureUrl', '0', 'picUrl' ], ''), // 主图
      originalPrice: data.commodityInfo.snPrice, // 原价
      actualPrice: data.commodityInfo.commodityPrice, // 卷后价(真实价格)
    }));
  }
}
