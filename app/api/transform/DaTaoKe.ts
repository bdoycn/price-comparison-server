import { Platform } from '../../service/Mall/typing/Mall';

export default class DaTaoKeTransform {
  static superSearch(dataList: any) {
    return dataList.map(data => ({
      id: data.id,
      platform: Platform.TB, // 平台
      title: data.title, // 标题
      mainPic: data.mainPic.includes('https') ? data.mainPic : `https:${data.mainPic}`, // 主图 有可能出现没有 schema 的情况
      originalPrice: data.originalPrice, // 原价
      actualPrice: data.actualPrice, // 卷后价(真实价格)
    }));
  }
}
