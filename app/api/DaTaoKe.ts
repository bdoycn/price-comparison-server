import BaseApi from './BaseApi';
import DaTaoKeTransform from './transform/DaTaoKe';
import { get } from 'lodash';

export default class DaTaoKeApi extends BaseApi {
  private static instance: DaTaoKeApi;
  static getInstance() {
    if (!this.instance) this.instance = new DaTaoKeApi();
    return this.instance;
  }

  superSearch = (params: ISuperSearchParams) => {
    return this.request({
      url: 'https://openapi.dataoke.com/api/goods/list-super-goods',
      params,
    })
      .then(res => get(res, [ 'data', 'data', 'list' ], []))
      .catch(() => [])
      .then(DaTaoKeTransform.superSearch);
  };
}

interface ISuperSearchParams {
  appKey: string,
  version: string,
  type: 0 | 1 | 2,
  pageId: number,
  pageSize: number,
  keyWords: string,
  timer: number,
  nonce: string,
  signRan: string,
}
