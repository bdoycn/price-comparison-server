import BaseApi from './BaseApi';
import SNTransform from './transform/SN';
import { get } from 'lodash';

export default class SNApi extends BaseApi {
  private static instance: SNApi;
  static getInstance() {
    if (!this.instance) this.instance = new SNApi();
    return this.instance;
  }

  search = (headers: ISearchHeadersParams, data: ISearchDataParams) => {
    return this.request({
      url: 'https://open.suning.com/api/http/sopRequest',
      headers,
      data,
      method: 'POST',
    })
      .then(res => get(res, [ 'data', 'sn_responseContent', 'sn_body', 'querySearchcommodity' ], []))
      .catch(() => [])
      .then(SNTransform.search);
  };
}

interface ISearchHeadersParams {
  appMethod: string,
  appRequestTime: string,
  format: 'xml' | 'json',
  appKey: string,
  versionNo: string,
  signInfo: string,
}

interface ISearchDataParams {
  keyword: string,
  pageIndex: number,
  size: number,
}
