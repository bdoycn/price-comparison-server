import BaseApi from './BaseApi';
import PDDTransform from './transform/PDD';
import { get } from 'lodash';

export default class PDDApi extends BaseApi {
  private static instance: PDDApi;
  static getInstance() {
    if (!this.instance) this.instance = new PDDApi();
    return this.instance;
  }

  search = (params: ISuperSearchParams) => {
    return this.request({
      url: 'https://gw-api.pinduoduo.com/api/router',
      params,
    })
      .then(res => get(res, [ 'data', 'goods_search_response', 'goods_list' ], []))
      .catch(() => [])
      .then(PDDTransform.search);
  };
}

interface ISuperSearchParams {
  appKey: string,
  version: string,
  type: 0 | 1 | 2,
  pageId: number,
  pageSize: number,
  keyWords: string,
  timer: number,
  nonce: string,
  signRan: string,
}
