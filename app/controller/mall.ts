import { Controller } from 'egg';
import { Platform } from '../service/Mall/typing/Mall';

export default class MallController extends Controller {
  public async search() {
    const { ctx } = this;

    if (!ctx.query.searchContent) return new Error('请输入搜索内容');

    const query = ctx.query as SearchQuery;

    const params = {
      searchContent: query.searchContent,
      page: query.page || 1,
      pageSize: 10,
      platforms: query.platforms || [ Platform.TB, Platform.JD, Platform.PDD, Platform.SN ],
    };
    ctx.body = await ctx.service.mall.queryList(params);
  }

  public async searchByPlatform() {
    const { ctx } = this;

    if (!ctx.query.searchContent) return new Error('请输入搜索内容');
    if (!ctx.params.platform) return new Error('请传入对应平台');

    const query = ctx.query as SearchByPlatformQuery;
    const { platform } = ctx.params as SearchByPlatformParams;

    const params = {
      searchContent: query.searchContent,
      page: query.page || 1,
      pageSize: 10,
      platform,
    };
    ctx.body = await ctx.service.mall.queryListByPlatform(params);
  }
}

type SearchQuery = {
  searchContent: string,
  page: number,
  platforms: Platform[],
};

type SearchByPlatformParams = {
  platform: Platform,
};

type SearchByPlatformQuery = {
  searchContent: string,
  page: number,
};
