import { GoodsDetail } from './typing/Mall';

export default abstract class BaseMall {
  abstract async queryList(params: IQueryListParams<any>): Promise<GoodsDetail[]>;
  abstract async queryDetail(params: IQueryDetailParams): Promise<GoodsDetail>;
}

export interface IQueryListParams<PlatformExtra> {
  searchContent: string,
  page: number,
  pageSize: number,
  platformExtra?: PlatformExtra,
}

export interface IQueryDetailParams {
  id: string | number,
}
