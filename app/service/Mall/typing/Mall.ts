// 淘宝 京东 拼多多 苏宁易购
export enum Platform {
  'TB' = 'TB',
  'JD' = 'JD',
  'PDD' = 'PDD',
  'SN' = 'SN',
}

export interface GoodsDetail {
  id: string | number, // id
  platform: Platform, // 平台
  title: string, // 标题
  mainPic: string, // 主图
  originalPrice: string, // 原价
  actualPrice: string, // 卷后价(真实价格)
}
