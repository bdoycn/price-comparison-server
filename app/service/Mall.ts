import { Service } from 'egg';
import PDDMall from './Mall/PDDMall';
import SNMall from './Mall/SNMall';
import { TBMall } from './Mall/TBMall';
import { GoodsDetail, Platform } from './Mall/typing/Mall';

const tbMall = new TBMall();
const pddMall = new PDDMall();
const snMall = new SNMall();

export default class MallService extends Service {
  async queryList(params: IQueryListParams) {
    const { searchContent, page, pageSize, platforms } = params;

    const getGoodsDetailPromiseByPlatform: Record<Platform, Promise<any[]>> = {
      [Platform.TB]: Promise.resolve([]),
      [Platform.JD]: Promise.resolve([]),
      [Platform.PDD]: Promise.resolve([]),
      [Platform.SN]: Promise.resolve([]),
    };

    const commonParamsBySinglePlatform = {
      page,
      pageSize,
      searchContent,
    };

    if (platforms.includes(Platform.TB)) {
      getGoodsDetailPromiseByPlatform.TB = tbMall.queryList(commonParamsBySinglePlatform);
    }
    if (platforms.includes(Platform.PDD)) {
      getGoodsDetailPromiseByPlatform.PDD = pddMall.queryList(commonParamsBySinglePlatform);
    }
    if (platforms.includes(Platform.SN)) {
      getGoodsDetailPromiseByPlatform.SN = snMall.queryList(commonParamsBySinglePlatform);
    }

    // 发送请求 获取数据
    const getGoodsDetailPromiseKeys = [] as Platform[];
    const getGoodsDetailPromiseValues = [] as Promise<any[]>[];
    Object.entries(getGoodsDetailPromiseByPlatform).forEach(([ key, value ]) => {
      getGoodsDetailPromiseKeys.push(key as Platform);
      getGoodsDetailPromiseValues.push(value);
    });
    const getGoodsDetailResults = await Promise.allSettled(getGoodsDetailPromiseValues);
    const goodsDetailByPlatform = getGoodsDetailResults.reduce((prev, result, index) => {
      let resolvedValue = [] as GoodsDetail[];
      if (result.status === 'fulfilled') resolvedValue = result.value;

      const platform = getGoodsDetailPromiseKeys[index];
      prev[platform] = resolvedValue;

      return prev;
    }, {} as Record<Platform, GoodsDetail[]>);

    return goodsDetailByPlatform;
  }

  async queryListByPlatform(params: IQueryListByPlatformParams) {
    const { searchContent, page, pageSize, platform } = params;

    const queryParams = {
      page,
      pageSize,
      searchContent,
    };

    let queryPromise: Promise<any>;

    switch (platform) {
      case Platform.TB:
        queryPromise = tbMall.queryList(queryParams);
        break;
      case Platform.PDD:
        queryPromise = pddMall.queryList(queryParams);
        break;
      case Platform.SN:
        queryPromise = snMall.queryList(queryParams);
        break;
      case Platform.JD:
      default:
        queryPromise = Promise.resolve([]);
    }

    return queryPromise;
  }
}

interface IQueryListParams {
  searchContent: string,
  page: number,
  pageSize: number,
  platforms: Platform[],
  platformsExtra?: {
    [Platform.TB]: any,
    [Platform.JD]: any,
    [Platform.PDD]: any,
    [Platform.SN]: any,
  },
}

interface IQueryListByPlatformParams {
  searchContent: string,
  page: number,
  pageSize: number,
  platform: Platform,
  platformExtra?: any,
}
