import { Application } from 'egg';

export default (app: Application) => {
  const { controller, router } = app;

  router.get('/', controller.home.index);
  router.get('/mall', controller.mall.search);
  router.get('/mall/:platform', controller.mall.searchByPlatform);
};
